# Мортал Комбат Х.

### Описание

>Чтобы вы понимали, насколько я серьезен, я буду говорить на русском языке.

Это мой первый проект, моя первая игра которую я не постесняюсь таковой назвать. Я сделал ее в далеком 2014 году, на первом курсе института, в качестве курсовой работы. 
Конечно, сейчас при взгляде на нее понятно, что код далек от идеала (мягка говоря), но я все еще горжусь
этим проектом и буду гордиться им всегда. 

Почему? Он отражает суть меня, моего естества. Я едва начинал постигать парадигмы ООП, не знал ничего про графику, про dll, lib, каким образом подключать
что либо к vs. 

**Но у меня была четкая цель - сделать что-то крутое!** 

Без всяких отговорок вроде - ну это первый курс, мы только паскаль прошли, без уверток и ужимок просто
взять и сделать круто. И я выложился на все сто, я сделал по тем временам гигантскую работу. Конечно, сейчас
я мог бы повторить все это за день, но гордость за стойкость меня трехлетней давности не покинет меня от этого 
все равно. Далее факты

## Список фич:

- Три карты, использющие динамическую генерацию ландшафта (never as same as before)
- Два персонажа
- Стабильные 40 fps
- Анимированный задний фон
- Постоянно меняющиеся условия погоды
- Звуки ударов и музыка на заднем фоне
- Удары ногой и рукой
- Использование подгружаемых текстур, ради кражи меню оригинала
- Кастомизируемый уровень кровавости
- Фаталити
- Имитация 3д за счет отрисовки полигонов тел
- Неограниченный экран

## Скриншоты:

![Меню](images/main.png) 

![Ромашки](images/1.jpg)

![Сакура](images/4.jpg)

![Фаталити](images/3.jpg)