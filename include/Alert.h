#pragma once

#include "defs.h"
#include "Text\Text.h"
#include "myMessageBox.h"
#include <glut.h> 
/*������ glut ������ ��������� 
fatal error C1189: #error :  gl.h included before glew.h 
Text ���������� ��, ��� ����� ���������� ������*/

class Alert
{
private:
	std :: shared_ptr<Text> needBuy;
	std :: shared_ptr<myMessageBox> messageNeedBuy;
public:
	Alert();

	void load();

	void drawNeedBuy(const double x,const double y,const double angle) const;

	void drawMessageNeedBuy(const double x,const double y,const double angle) const;
};

