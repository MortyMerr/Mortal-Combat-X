#include "BackgroundSakura.h"


BackgroundSakura::BackgroundSakura()
{
}

BackgroundSakura::~BackgroundSakura()
{
}

void BackgroundSakura :: load()
{
	leftF = new SakuraFlow(100,20,20,0.06,-1);
	rightF = new SakuraFlow(100,gameWidth-20,20,0.08,1);
	leftF->load();
	rightF->load();

	glClearColor(1.0,1.0,1.0,1.0);
}

void BackgroundSakura :: draw() const
{
	glOval(gameWidth/2.0,gameHeight/2.0,100.0,100.0,myColor(1.0,0.0,0.0));

	leftF->draw();
	rightF->draw();
}

void BackgroundSakura :: correcting()
{
	leftF->stepForward();
	rightF->stepForward();
}

void sakura(double x, double y,double size,double angle,double xAxis,double yAxis,double zAxis)
{
	double a = 300.0, b = 60.0, l = 0.0;

	myColor start = myColor(255.0,20.0,105.0);
	myColor end = myColor(255.0,255.0,255.0);
	
	glPushMatrix();
	glTranslated(x,y,0.0);
	glRotated(angle,xAxis,yAxis,zAxis);
	glScaled(size,size,0.0);

	glBegin(GL_POLYGON);
	glColor3f(1.0,0.0,0.0);
		for (double t = -PI/3.0; t <= 0.09; t += 0.1)
		{
			b += 6.1;
			//glColor3f(1.0 - random(1000)/1000.0 + 0.01*t,1.0-random(100)/1000.0,1.0-random(100)/1000.0);
			glColor(start.getPart(l,end));
			glVertex2d(a*cos(t),b*sin(t));
			l += 1.0/12.0;
		}

		for (double t = 0.0; t <= PI/3.0 + 0.09; t += 0.1)
		{
			b -= 6.1;
			//glColor3f(1.0 - random(1000)/1000.0 + 0.01*t,1.0-random(100)/1000.0,1.0-random(100)/1000.0);
			glColor(start.getPart(l,end));
			glVertex2d(a*cos(t),b*sin(t));
			l -= 1.0/12.0;
		}
			
		b = 150.0;
		l = 0.0;
		
		for (double t = PI/3.0; t < PI + 0.09; t += 0.1)
		{
			//glColor3f(1.0 - random(1000)/1000.0 + 0.01*t,1.0-random(100)/1000.0,1.0-random(100)/1000.0);
			glColor(start.getPart(l,end));
			glVertex2d(a*cos(t) + a/80.0*cos(50*t*t*t),b*sin(t) + a/80.0*sin(50*t*t*t));
			l += 1.0/22.0;
		}
		
		for (double t =  PI; t < 5*PI/3.0; t += 0.1)
		{
			//glColor3f(1.0 - random(1000)/1000.0 + 0.01*t,0.3-random(100)/1000.0,0.3-random(100)/1000.0);
			glColor(start.getPart(l,end));
			glVertex2d(a*cos(t) + a/80.0*cos(50*t*t*t),b*sin(t) + a/80.0*sin(50*t*t*t));
			l -= 1.0/21.0;
		}

	glEnd();

	glColor(myColor(255.0, 0.0, 255.0));

	/*glBegin(GL_LINES);
		glVertex2d(200.0,20.0);
		for (int i = 0; i < 15; i++)
		{
			glVertex2d(200.0 + random(15),-10 + random(20));
			glVertex2d(-250.0 + random(50),-80 + random(160));
		}
	glEnd();*/

	glPopMatrix();
}

double flight(double t,char&& ch,const int& dir)
{
	double a = 80.0, b = 10.0, v = 8.0;
	switch (ch)
	{
	case 'x':
		{
			if( t > 10.0)
			{
				if (dir == -1)
					return 563.0 + a*cos(t);
				else
					return 1000 - (563.0 + a*cos(t - 2.0));
			} else
			{
				if (dir == -1)
					return 50.0*t;
				else
					return 1000-50.0*t;
			}
		};
	case 'y':
		{
			if( t > 10.0)
			{
				if (dir == -1)
					return 190.4 + b*sin(t) + v*t;
				else
					return  190.4 + b*sin(t) + v*t;
			} else
			{
				if (dir == -1)
					return 100.0 + pow(E,0.5*t);
				else
					return 100.0 + pow(E,0.5*(t));
			}
		}
	};
	return 0;
}

double fall(double startX,double startY,double x)
{
	return -(x-startX)+startY; 
}