#pragma once
#include <windows.h>
#include <time.h>
#include <math.h>
#include <glut.h>
#include "defs.h"

#include<vector>

class blood{
	public:
		float angle, speed, vX, curAngle,vY,v0Y;
		double radius,length,size,time,xHit,yHit,step;

		blood(){};

		blood(int x,int y):
		length(50.0), step(0.4), size(0.2), xHit(x), yHit(y), angle(random(157000)/1000),
		speed( 4+random(10)),  time(0)
		{
			radius = (sqrt(0.8*length) - straight(0.8*length))/2;
		}

		void rerandom(int x,int y);
		
		double random(int a);

		float straight(float x);

		float circleFunc(float x,float x0,float r);

		void part(double tAngle);

		void pole();
};

class moreBlood
{
public:
	int howMuchBlood,minY, limitOfBlood;
	bool fatal;
	std :: vector<blood*> washOfBlood;
	
	moreBlood():howMuchBlood(100),minY(100), fatal(false), limitOfBlood(0), washOfBlood(1000)
	{
		for(int i = 0;i < 1000; ++i)
			washOfBlood[i]= new blood(300,300);
	}

	void fatality();
	
	void pole();

	void rerandom(int x,int y);
};

