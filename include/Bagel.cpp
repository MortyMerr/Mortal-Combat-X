#include "Bagel.h"

Bagel :: Bagel(const heroType& a, int t, bool l):
	MainHero(a,t,l)
{}

void Bagel :: draw() const
{
	drawFeatures();

	MainHero :: draw();
}

void Bagel :: drawFeatures() const
{
	double i;
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
		glBegin(GL_POLYGON);
		if(left) 
			for(i=0;i<6.28;i+=0.01) 
			{
				glVertex2d(coordOfBody.x+identity.widthOfBody*cos(i),coordOfBody.y+identity.hightOfBody*sin(i));
				glColor3f(identity.color.red-i*0.2,identity.color.green-i*0.2,identity.color.bidlo-i*0.2);
			} else 
				for(i=6.28;i>0;i-=0.01) 
				{
					glVertex2d(coordOfBody.x+identity.widthOfBody*cos(i),coordOfBody.y+identity.hightOfBody*sin(i));
					glColor3f(identity.color.red-i*0.2,identity.color.green-i*0.2,identity.color.bidlo-i*0.2);
				};
			glEnd();
			glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
			glBegin(GL_POLYGON);
			if(left)
				for(i=0;i<6.28;i+=0.01)
				{
					glVertex2d(coordOfBody.x+identity.HeadRad*cos(i),coordOfBody.y+identity.hightOfBody+identity.HeadRad-5+identity.HeadRad*sin(i));
					glColor3f(identity.color.red-i*2.1,identity.color.green-i*0.2,identity.color.bidlo-i*0.2);
				}
			else
				for(i=6.28;i>0;i-=0.01)
				{
					glVertex2d(coordOfBody.x+identity.HeadRad*cos(i),coordOfBody.y+identity.hightOfBody+identity.HeadRad-5+identity.HeadRad*sin(i));
					glColor3f(identity.color.red-i*0.2,identity.color.green-i*0.2,identity.color.bidlo-i*0.2);
				}
				glEnd();
	printBeard();
}

void Bagel :: printBeard() const
{
	glColor3f(1.0,1.0,1.0);
	glBegin(GL_POLYGON);
	glColor3f(1.0,1.0,1.0);
	for(double i=0.8;i<5.6;i+=0.01) 
		glVertex2d(coordOfBody.x-8+13*cos(i),coordOfBody.y+20*sin(i));
	for(double i=4;i<5.8;i+=0.01) 
		glVertex2d(coordOfBody.x+8+13*cos(i),coordOfBody.y+20*sin(i));
	for(double i=4.4;i>2;i-=0.01) 
		glVertex2d(coordOfBody.x+23+9*cos(i),coordOfBody.y+9*sin(i));
	for(double i=0.5;i<2.4;i+=0.01) 
		glVertex2d(coordOfBody.x+8+13*cos(i),coordOfBody.y+20*sin(i));
	glEnd();
	glBegin(GL_POLYGON);
	for(double i=4.7;i<6;i+=0.01) 
		glVertex2d(coordOfBody.x+4+7*cos(i),coordOfBody.y+28+7*sin(i));
	for(double i=1;i<3;i+=0.01) 
		glVertex2d(coordOfBody.x+8+7*cos(i),coordOfBody.y+20+7*sin(i));

	glEnd();

	glBegin(GL_LINES);
	for(double i=5;i<6.0;i+=0.2) 
	{
		glColor3f(139.0/255, 69.0/255, 19.0/255);
		glVertex2d(coordOfBody.x+rightFaced*identity.HeadRad*cos(i),coordOfBody.y+identity.hightOfBody+identity.HeadRad-5+identity.HeadRad*sin(i)+2);
		glColor3f(139.0/255,129.0/255, 76.0/255);	
		glVertex2d(coordOfBody.x+rightFaced*identity.HeadRad*cos(i),coordOfBody.y+identity.hightOfBody+identity.HeadRad-5+identity.HeadRad*sin(i)-10);
	}
	glEnd();
}
