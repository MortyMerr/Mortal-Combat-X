#pragma once

#include "MainHero.h"

class Curly : public MainHero
{
public:
	Curly(const heroType& a, int t, bool l);

	void draw() const;

	void drawFeatures() const;
private:
	void printHair() const;
};

