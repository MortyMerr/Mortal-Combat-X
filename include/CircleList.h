#pragma once

template<class T>
struct cell
{
	T inf;
	cell* next;
};

template<class T>
class CircleList
{
private:
	cell<T>* last;
public:
	CircleList();
	~CircleList();
	void pushBack(T value);
	void toNext();
	const T getInf() const; 
};

