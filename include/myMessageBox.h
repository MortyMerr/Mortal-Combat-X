#pragma once

#include "Text\Text.h"
#include "myColor.h"

class myMessageBox
{
public:
	myMessageBox();
	void draw(const double width,const double height) const;

	void load();
private:
	std :: shared_ptr<Text> messageText;
	std :: shared_ptr<Text> tittleText;
	std :: shared_ptr</*const*/ Text> okText;
};

