#include "BackgroundChild.h"


BackgroundChild::BackgroundChild():
	st(day)
{
}


BackgroundChild::~BackgroundChild()
{
}

void BackgroundChild :: load()
{
	s = std :: shared_ptr<Sun>(new Sun());
	bC = std :: shared_ptr<BackColor> (new BackColor(myColor(0.0,154.0,205.0),myColor(164.0, 211.0, 238.0),
					myColor(255.0,114.0,86.0),myColor(55.0,160.0,122.0),1000));
	s->setX(10);
	s->setY(100);
	s->setS(40);
	s->setR(30);
	m = nullptr;

	pOC = std :: shared_ptr<PoleOfCamomile>(new PoleOfCamomile(20));
}

void BackgroundChild :: draw() const
{
	glBegin(GL_POLYGON);
			glColor(bC->getCurrentDownColor());
			glVertex2d(0.0,0.0);
			glColor(bC->getCurrentUpColor());
			glVertex2d(0.0,gameHeight);
			glColor(bC->getCurrentUpColor());
			glVertex2d(gameWidth,gameHeight);
			glColor(bC->getCurrentDownColor());
			glVertex2d(gameWidth,0.0);

	glEnd();
	if (s)
	{
		s->draw();
		s->stepForward();
	}
	else
	{
		m->stepForward();
		m->draw();
	}
	
	glBegin(GL_LINES);
			for(double i = 0;i < gameWidth; i += 1.0)
			{
				glColor3d(0.0,1.0,0.0);
				glVertex2d(i,0.0);
				glColor3d(0.4,1.0,0.4);
				glVertex2d(i,greenFunc(i));
			}
	glEnd();
	
	pOC->draw();
}

void BackgroundChild :: correcting()
{
	switch(st)
	{
	case day:
		{
			if (isEndOfDay())
			{
				s = nullptr; //��� ��������� ������
				m = std :: shared_ptr<Moon> (new Moon());
				m->setX(10);
				m->setY(100);
				m->setS(40);
				m->setR(30);
				st = night;
				bC->recolorForNight();
			}
			break;
		};
	case night:
		{
			if(isEndOfNight())
			{
				m = nullptr;
				s = std :: shared_ptr<Sun> (new Sun());
				s->setX(10);
				s->setY(100);
				s->setS(40);
				s->setR(30);
				st = day;
				bC->recolorForDay();
			}
			break;
		};
	}
}

bool BackgroundChild :: isEndOfDay() const
{
	return s->getX() > gameWidth;
}
bool BackgroundChild :: isEndOfNight() const
{
	return m->getX() > gameWidth;
}


double sunFunc(double x)
{
	//sin
	//return 400+20*sin(x/20);    // ��� ��� /100 �������� ������ ������������

	//sqr
	return -0.001*pow(x-gameWidth/2.0,2.0) + gameHeight-50.0;
}

double circleFunc(double x,double x0,double r)
{
	return sqrt(r*r - (x-x0)*(x-x0));
}


double greenFunc(double x)
{
	/*x /= 10;
	return ((4*x*x - 6*x*x + 1)*sqrt(x+1))/(3-x);*/

	//return 10*sqrt(x);

	return 200.0;
}