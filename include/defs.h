#pragma once

#define gameWidth 1000.0

#define gameHeight 700.0

#define PI 3.14159265359

#define E 2.71828182846

#define heroCount 2

#define backCount 3

#define heroDescription "Description/heroDescription.txt"
#define backDescription "Description/backDescription.txt"

#define heroScript "heroScript/source.txt"