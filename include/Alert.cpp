#include "Alert.h"


Alert::Alert()
{
	needBuy = nullptr;
}

void Alert :: load()
{
	messageNeedBuy = std :: shared_ptr<myMessageBox>(new myMessageBox());
	messageNeedBuy->load();
	needBuy = std :: shared_ptr<Text>(new Text());
	needBuy->setColor(1.0,0.0,0.0,1.0);
	needBuy->setFont("font/Ubuntu-R.ttf",18);
	needBuy->setText("It is beta version\n to use this option buy pro version\n for 25$");
	needBuy->setX(0);
	needBuy->setY(0);
}

void Alert :: drawNeedBuy(const double x,const double y,const double angle) const
{
	if (needBuy != nullptr)
	{
		glPushMatrix();
		glTranslated(x,y,0.0);
		glScaled(2.0,2.0,1.0);
		glRotated(180.0*angle/PI,0.0,0.0,-1);

		needBuy->draw();

		glPopMatrix();
	}
}

void Alert :: drawMessageNeedBuy(const double x,const double y,const double angle) const
{
	glPushMatrix();
	glTranslated(x,y,0.0);
	glScaled(2.0,2.0,1.0);
	glRotated(180.0*angle/PI,0.0,0.0,-1);

	messageNeedBuy->draw(300,100);

	glPopMatrix();
}