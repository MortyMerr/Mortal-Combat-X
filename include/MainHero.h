#pragma once
#include "Text\Text.h"
#include <iostream>
#include "moreBlood.h"
#include "myColor.h"

struct heroColor
	{
		heroColor(){};
		double red, green, bidlo;
		heroColor(double r, double g, double b): 
		red(r/255.0), green(g/255.0), bidlo(b/255.0){};
    };

struct heroType
{
	const double lengthOfLeg, hightOfBody, widthOfBody, lengthOfHand, HeadRad;
	double speedOfHit;
	int startHealth, speedOfWalking;
	heroColor color;
	std :: string name;
	heroType(double a, double b, double c,double d, double e, int f, double g, heroColor h, int speed, std :: string s):
		lengthOfLeg(a),
		hightOfBody(b),
		widthOfBody(c),
		color(h), 
		speedOfWalking(speed),
		lengthOfHand(d),
		HeadRad(e), 
		startHealth(f), 
		speedOfHit(g),
		name(s)
	{}
};
struct coord
{
	double x,y;
};

class MainHero
{
protected:
	int rightFaced, inHit, inHitLeg, nowHealth;
	bool leftLegStep,rightLegStep,inJump, left, bleeding;
	heroType identity; 
	coord coordLeftLeg, coordRightLeg, coordRightHand, coordOfBody;
	double cornerLeft,cornerRight, cornerHittingHand, startLegCorner,  startLegX , cornerHittingLeg,speed0;
	const double startLeftY;
	int countSound;
	std :: shared_ptr<Text> nameDrawer;
public:
	MainHero();
	
	double ourTime;
	
	MainHero(const heroType& a, int t, bool l);

	virtual void load();

	void drawName(const double&& x, const double&& y) const;
	
	bool checkHit(MainHero& pig, coord temp);//��������� ������ �� ��� ����� - pig'�
	bool getInJump();
	bool getInHit();
	bool getInHitLeg();
	void stepRight(MainHero& anotherHero);
	void stepRight();
	void test();
	void stepLeft(MainHero& anotherHero);
	void stepLeft();
	void jump();
	void hitHand();
	void hitLeg();
	void reverse(MainHero& anotherHero);
	void changeHealth(int a);
	int getGameWidth();
	int getHealth();
	int getStartHealth();
	const int checkDistance(MainHero& anotherHero) const;

	const std :: string getName() const;

	const bool isBleeding() const;

	void correcting();
	void correcting(MainHero& anotherHero);
	virtual void draw() const;
	virtual void drawFeatures() const;

	void setBlood(std :: shared_ptr<moreBlood> b);

	~MainHero();
private:
	void drawBlood() const;
	std :: shared_ptr<moreBlood> bloody;
};

