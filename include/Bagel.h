#pragma once

#include "MainHero.h"

class Bagel : public MainHero
{
public:
	Bagel(const heroType& a, int t, bool l);

	void draw() const;

	void drawFeatures() const;
private:
	void printBeard() const;
};