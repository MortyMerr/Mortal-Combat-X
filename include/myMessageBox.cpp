#include "myMessageBox.h"


myMessageBox :: myMessageBox()
{
	messageText = nullptr;
	tittleText = nullptr;
	okText = nullptr;
}

void myMessageBox :: draw(const double width,const double height) const
{
	if (messageText != nullptr)
	{
		const double RECT_IN_X_START = 0.01,
			RECT_IN_X_END = 0.99,
			RECT_IN_Y_START = 0.05,
			RECT_IN_Y_END = 0.85;
		const double X_POS_START = 0.96, 
			X_POS_END = 0.99, 
			Y_POS_START = 0.9,
			Y_POS_END = 1.0, 
			X_WIDTH = width*(X_POS_END-X_POS_START),
			X_HEIGHT = height*(Y_POS_END - Y_POS_START);
		const double LINE_WIDTH = 2.0;
		const unsigned int X_PARTS = 6;
		const double RECT_OK_X_START = 0.6,
			RECT_OK_X_END = 0.8,
			RECT_OK_Y_START = 0.1,
			RECT_OK_Y_END = 0.3;

		glBegin(GL_POLYGON);
			/*������������� - ������*/
			glColor(myColor(100.0, 149.0, 237.0));	
			glVertex2d(0.0,0.0);
			glVertex2d(width,0.0);	
			glColor(myColor(245.0,245.0,245.0));
			glVertex2d(width,height);
			glVertex2d(0.0,height);
			/************************/
		glEnd();

		glColor(myColor(0.0, 0.0, 0.0));
		glLineWidth(LINE_WIDTH);
		glBegin(GL_LINE_LOOP);
			/*�������������-������-������*/
			glVertex2d(0.0,0.0);
			glVertex2d(width,0.0);	
			glVertex2d(width,height);
			glVertex2d(0.0,height);
			/*****************************/
		glEnd();

		glBegin(GL_POLYGON);
			/*������������� - ����������*/
			glColor(myColor(0.0, 255.0, 255.0));	
			glVertex2d(RECT_IN_X_START*width,RECT_IN_Y_START*height);
			glVertex2d(width*RECT_IN_X_END,RECT_IN_Y_START*height);	
			glColor(myColor(256.0,256.0,256.0));
			glVertex2d(width*RECT_IN_X_END,RECT_IN_Y_END*height);
			glVertex2d(RECT_IN_X_START*width,RECT_IN_Y_END*height);
			/****************************/
		glEnd();

		glColor(myColor(0.0, 0.0, 0.0));
		glLineWidth(LINE_WIDTH);
		glBegin(GL_LINE_LOOP);
			/*�������������-����������-������*/	
			glVertex2d(RECT_IN_X_START*width,RECT_IN_Y_START*height);
			glVertex2d(width*RECT_IN_X_END,RECT_IN_Y_START*height);	
			glVertex2d(width*RECT_IN_X_END,RECT_IN_Y_END*height);
			glVertex2d(RECT_IN_X_START*width,RECT_IN_Y_END*height);
			/*****************************/
		glEnd();

		glBegin(GL_POLYGON);
			/*������������� - �������*/
			glColor(myColor(255.0,0.0,0.0));	
			glVertex2d(X_POS_START*width,Y_POS_START*height);
			glVertex2d(width*X_POS_END,Y_POS_START*height);	
			glColor(myColor(205.0, 92.0, 92.0));
			glVertex2d(width*X_POS_END,height*Y_POS_END);
			glVertex2d(X_POS_START*width,height*Y_POS_END);
			/*************************/
		glEnd();

		glColor(myColor(0.0, 0.0, 0.0));
		glLineWidth(LINE_WIDTH);
		glBegin(GL_LINE_LOOP);
			/*�������������-�������-������*/	
			glVertex2d(X_POS_START*width,Y_POS_START*height);
			glVertex2d(width*X_POS_END,Y_POS_START*height);	
			glVertex2d(width*X_POS_END,height*Y_POS_END);
			glVertex2d(X_POS_START*width,height*Y_POS_END);
			/*****************************/
		glEnd();

		glColor(myColor(255.0, 255.0, 255.0));
		glBegin(GL_LINE_LOOP);
			/*�������*/
			glVertex2d(X_POS_START*width + 1.0/X_PARTS*X_WIDTH,Y_POS_START*height + 1.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 2.0/X_PARTS*X_WIDTH,Y_POS_START*height + 1.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 3.0/X_PARTS*X_WIDTH,Y_POS_START*height + 2.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 4.0/X_PARTS*X_WIDTH,Y_POS_START*height + 1.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 5.0/X_PARTS*X_WIDTH,Y_POS_START*height + 1.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 4.0/X_PARTS*X_WIDTH,Y_POS_START*height + 3.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 5.0/X_PARTS*X_WIDTH,Y_POS_START*height + 5.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 4.0/X_PARTS*X_WIDTH,Y_POS_START*height + 5.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 3.0/X_PARTS*X_WIDTH,Y_POS_START*height + 4.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 2.0/X_PARTS*X_WIDTH,Y_POS_START*height + 5.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 1.0/X_PARTS*X_WIDTH,Y_POS_START*height + 5.0/X_PARTS*X_HEIGHT);
			glVertex2d(X_POS_START*width + 2.0/X_PARTS*X_WIDTH,Y_POS_START*height + 3.0/X_PARTS*X_HEIGHT);
			//glVertex2d(X_POS_START*width + 1.0/X_PARTS*X_WIDTH,Y_POS_START*height + 1.0/X_PARTS*X_HEIGHT);
			/*********/
		glEnd();

		glBegin(GL_POLYGON);
			/*������������� - ��*/
			glColor(myColor(255.0, 99.0, 71.0));	
			glVertex2d(RECT_IN_X_START*width + width*(1.0 - 2*RECT_IN_X_START)*RECT_OK_X_START,RECT_IN_X_START*height + height*(1.0 - 2*RECT_IN_X_START)*RECT_OK_Y_START);
			glVertex2d(RECT_IN_X_START*width + width*(1.0 - 2*RECT_IN_X_START)*RECT_OK_X_END,RECT_IN_X_START*height + height*(1.0 - 2*RECT_IN_X_START)*RECT_OK_Y_START);	
			glColor(myColor(255.0, 69.0, 0.0));
			glVertex2d(RECT_IN_X_START*width + width*(1.0 - 2*RECT_IN_X_START)*RECT_OK_X_END,RECT_IN_X_START*height + height*(1.0 - 2*RECT_IN_X_START)*RECT_OK_Y_END);
			glVertex2d(RECT_IN_X_START*width + width*(1.0 - 2*RECT_IN_X_START)*RECT_OK_X_START,RECT_IN_X_START*height + height*(1.0 - 2*RECT_IN_X_START)*RECT_OK_Y_END);
			/********************/
		glEnd();

		/*������*/
		glTranslated(width*RECT_IN_X_START,height*RECT_IN_Y_END*RECT_IN_Y_END,0.0);
		tittleText->draw();
		glTranslated(-width*RECT_IN_X_START,-height*RECT_IN_Y_END*RECT_IN_Y_END,0.0);

		glTranslated(width*RECT_IN_X_START,height*RECT_IN_Y_END*0.5,0.0);
		messageText->draw();
		glTranslated(-width*RECT_IN_X_START,-height*RECT_IN_Y_END*0.5,0.0);

		glTranslated(RECT_IN_X_START*width + width*(1.08 - 2*RECT_IN_X_START)*RECT_OK_X_START,RECT_IN_X_START*height*0.1 + height*(0.3 - 16*RECT_IN_X_START)*RECT_OK_Y_START,0.0);
		okText->draw();
		/********/
	}
}

void myMessageBox :: load()
{
	messageText = std :: shared_ptr<Text>(new Text());
	messageText->setColor(1.0,0.0,0.0,1.0);
	messageText->setFont("font/Ubuntu-R.ttf",10);
	messageText->setText("It is beta version\n to use this option buy pro version\n for 25$");
	messageText->setX(0);
	messageText->setY(0);
	messageText->setLineSpacing(0);
	messageText->setLetterSpacing(0);

	tittleText = std :: shared_ptr<Text>(new Text());
	tittleText->setColor(0.0,0.0,0.0,1.0);
	tittleText->setFont("font/Ubuntu-R.ttf",18);
	tittleText->setText("ERROR");
	tittleText->setX(0);
	tittleText->setY(0);

	okText = std :: shared_ptr<Text>(new Text());
	okText->setColor(0.0,0.0,0.0,1.0);
	okText->setFont("font/Ubuntu-R.ttf",18);
	okText->setText("OK");
	okText->setX(0);
	okText->setY(0);
}