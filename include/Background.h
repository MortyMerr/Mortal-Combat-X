#pragma once

#include "MainHero.h"	
#include "defs.h"

int random(int&& m);
int random(int& m);
void glOval(double,double,double,double,myColor);

class Background
{

public:
	
	const int&& printHealthAndName(MainHero& first, MainHero& second) const;
	
	void printWin(MainHero& winner) const;
	
	virtual void correcting();

	virtual void load();
	
	virtual void draw() const;

	Background();
	
	~Background();
};

