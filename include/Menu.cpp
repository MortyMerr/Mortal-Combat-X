#include "Menu.h"


Menu::Menu():
	position(0),
	actionFlag(false)
{	
	files[0] = L"picture/menuBack0.bmp";
	files[1] = L"picture/menuBack1.bmp";
	files[2] = L"picture/menuBack2.bmp";
	files[3] = L"picture/menuBack3.bmp";
	files[4] = L"picture/menuBack4.bmp";
	files[5] = L"picture/menuBack5.bmp";
	files[6] = L"picture/menuBack6.bmp";
	files[7] = L"picture/menuBack7.bmp";

	sliderFile = "picture/slider.png";
}


Menu::~Menu()
{
}

void Menu :: load()
{
	unsigned char* data;
	unsigned width, height;

	slider = lodepng_decode32_file(&data, &width, &height, sliderFile);
	glGenTextures(1,&slider);
	glBindTexture(GL_TEXTURE_2D,slider);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);

	delete data;

	for (int i = 0; i < 8; i++)
		image[i] = auxDIBImageLoad(files[i]);

	backMas[0] = std :: shared_ptr<Background>( new Background()); backMas[0]->load();
	backMas[1] = std :: shared_ptr<BackgroundChild>(new BackgroundChild()); backMas[1]->load();
	backMas[2] = std :: shared_ptr<BackgroundSakura>(new BackgroundSakura()); backMas[2]->load();

	heroColor kudrishColor(124, 252, 0);
	heroColor bublickColor(135, 206, 235);

	heroType first(4,40,100,60,15,200,0.04, kudrishColor,3,"������");//�������, �������� �����, ������ �����, ����� ����, ����� �������, ��������
	heroType second(70,50,20,40,30,100,0.1, bublickColor,5,"������");//�������, �����������, �������� �����, ������ ����, ������ �������, ������

	heroMas[0] = std :: shared_ptr<MainHero>(new Bagel(first, 1, true));
	heroMas[1] = std :: shared_ptr<MainHero>(new Curly(second, 1, false));

	std :: ifstream in(backDescription);
	/****loadDescription*****/
	for (int i = 0; i < backCount + heroCount; i++)
	{

		backText[i] = std :: shared_ptr<Text>(new Text());
		backText[i]->setFont("font/Times_New_Roman.ttf",18);
	}

	std :: string temp;
	int del,len;

	for (int i=0; i < backCount; i++)
	{
		backText[i]->setX(500);
		backText[i]->setY(500);

		getline(in,temp);

		/***insert /n for beauty***/
		len = temp.length();
		del = len / 40;
		for (int j = 1; j <= del; ++j)
		{
			int p = 40*j;
			while ((p < len) && (temp[p] != ' '))
				p++;
			if (p < len)
				temp[p] = '\n';
		}
		/**************************/

		backText[i]->setLineSpacing(1);
		backText[i]->setText(temp);
		backText[i]->setColor(0.2,0.3,0.1,1.0);
	}

	in.close();
	in.open(heroDescription);

	for (int i=0; i < heroCount; i++)
	{
		getline(in,temp);

		/***insert /n for beauty***/
		len = temp.length();
		del = len / 40;
		for (int j = 1; j <= del; ++j)
		{
			int p = 40*j;
			while ((p < len) && (temp[p] != ' '))
				p++;
			if (p < len)
				temp[p] = '\n';
		}
		/**************************/

		backText[backCount + i]->setX(500);
		backText[backCount + i]->setY(100);
		backText[backCount + i]->setLineSpacing(1);
		backText[backCount + i]->setText(temp);
		backText[backCount + i]->setColor(0.0,0.0,0.0,1.0);
	}

	/************************/
	in.close();

	in.open(heroScript);

	in >> scriptAmount;

	getline(in,temp);

	script = new CircleList<char>[scriptAmount];
	curScript = -1;

	for (int i = 0; i < scriptAmount; i++)
	{
		getline(in,temp);
		len = temp.length();
		for (int j = 0; j < len; j += 2)
			script[i].pushBack(temp[j]);
	};

	in.close();


	/***alerts loading***/
	alert.load();
	/********************/

	/***choosing loading***/
	for (int i = 0; i < 2; i++)
	{
		choosing[i] = std :: shared_ptr<Text>(new Text());
		choosing[i]->setColor(0.0,0.0,0.0,1.0);
		choosing[i]->setFont("font/Ubuntu-R.ttf",18);
	};
	choosing[0]->setText("Choosing first Hero");
	choosing[1]->setText("Choosing second Hero");
	/**********************/
}

void Menu :: moveDown()
{
	if ((position > 0) && (position < 8))
		position--;
}

void Menu :: moveUp()
{
	if (position >= 7)
		return;
	position++;
}

void Menu ::  draw() const
{
	if (position < 8) 
	{
		glDrawPixels(image[position]->sizeX, image[position]->sizeY,GL_RGB, GL_UNSIGNED_BYTE,image[position]->data);
		drawSlider();
	} else if (position == 20)
	{
		drawBackChoosing();
	} else if ((position == 21) || (position == 22))
	{
		drawHeroChosing();
	}
	if (countNeedBuy > 0.0) 
	{
		countNeedBuy -= 0.5;
		alert.drawMessageNeedBuy(330.0 + 100.0/30.0*countNeedBuy,58.0 + 100.0/30.0*countNeedBuy + position*36.0,PI/100.0*countNeedBuy);
	}
}

void Menu :: drawSlider() const
{
	int X1 = 0,Y1 = 0,X2= 1,Y2 = 1;

	glEnable( GL_TEXTURE_2D );
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glBindTexture( GL_TEXTURE_2D, slider);

	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 ); 
	glBegin( GL_QUADS );

	glTexCoord2i(X1,Y1); glVertex2d(330.0,58.0+position*36.0);
	glTexCoord2i(X1,Y2); glVertex2d(330.0,78.0+position*36.0);
	glTexCoord2i(X2,Y2); glVertex2d(350.0,78.0+position*36.0);
	glTexCoord2i(X2,Y1); glVertex2d(350.0,58.0+position*36.0);


	glEnd();

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
}

const int Menu :: getPosition() const
{
	return position;
}

const bool&& Menu :: isChangePageBackground() const
{
	return (page+1 < backCount);
}

const bool&& Menu :: isChangePageHero() const
{
	return (page+1 < heroCount);
}

const bool&& Menu :: isPageDown() const
{
	return (page > 0);
}

void Menu :: startChangeSettings()
{
	position = 20;
	page = 1;
}

void Menu :: drawBackChoosing() const
{
	backText[page]->draw();

	glPushMatrix();

	glScaled(0.5,0.5,0.5);

	backMas[page]->correcting();
	backMas[page]->draw();

	glPopMatrix();

	//

}

void Menu :: drawHeroChosing() const
{
	if ((position == 21) || (position == 22))
	{
		glPushMatrix();
		glTranslated(100.0,gameHeight-100.0,0.0);

		choosing[position-21]->draw();

		glPopMatrix();
	};
	
	backText[backCount + page]->draw();
	
	glPushMatrix();

	heroMas[page]->correcting();
	heroMas[page]->draw();

	glPopMatrix();

	//
}

void Menu :: toNextPage()
{
	switch(position)
	{
	case 20:
		{
			if(isChangePageBackground())
				page++;
			break;
		};
	case 21:
		{
			if (isChangePageHero())
				page++;
			break;
		};
	case 22:
		{
			if (isChangePageHero())
				page++;
			break;
		}
	}
}

void Menu :: toPrevPage()
{
	if (position > 7)
		if (isPageDown())
			page--;
}

void Menu :: action()
{
	actionFlag = true;
}

void Menu :: disAction()
{
	actionFlag = false;
}

const bool Menu :: isAction() const
{
	return actionFlag;
}

const int Menu :: startChoosingFirstHero()
{
	curScript = random(scriptAmount);
	position = 21;
	int temp(page);
	page = 0;
	return temp;
}

const int Menu :: startChoosingSecondHero()
{
	curScript = random(scriptAmount);
	position = 22;
	int temp(page);
	page = 0;
	return temp;
}

const int Menu :: endChoosing()
{
	position = 0;
	int temp(page);
	page = 0;
	return temp;
}

void Menu :: correcting() const
{
	if ((position == 21) || (position == 22))
	{
		switch(script[curScript].getInf())
		{
		case 'd':
			{
				heroMas[page]->stepRight();
				script[curScript].toNext();
				break;
			};
		case 'a':
			{
				heroMas[page]->stepLeft(); 
				script[curScript].toNext();
				break;
			};
		case 'w':
			{
				heroMas[page]->jump();
				script[curScript].toNext();
				break;
			};
		case 'e':
			{
				heroMas[page]->hitHand();
				script[curScript].toNext();
				break;
			};
		case 'f':
			{
				heroMas[page]->hitLeg();
				script[curScript].toNext();
				break;
			};
		}
	}
}

void Menu :: startNeedBuy() const
{
	countNeedBuy = 30.0;
}

void Menu :: endNeedBuy() const
{
	countNeedBuy = 0.0;
}