#pragma once

#include "Background.h"
#include "myColor.h"

#include <vector>

double fall(double,double,double);
double flight(double,char&&,const int&);
void sakura(double,double,double,double,double,double,double);

class Sakura
{
private:
	double startX;
	double x;
	double startY;
	double y;
	const double size;
	struct Axis
	{
		double x;
		double y;
		double z;
	} vector;
	double angle;
	void (*sakura)(double,double,double,double,double,double,double);
public:
	Sakura(double&& s,double& setStartX,double& setStartY) : 
	  size(s),
	  startX(setStartX),
	  startY(setStartY)
	{
		angle = random(360);
		
		vector.x = random(1000)/1000.0;
		vector.y = random(1000)/1000.0;
		vector.z = random(1000)/1000.0;
	}
	Sakura(double& s,double& setStartX,double&& setStartY) : 
	  size(s),
	  startX(setStartX),
	  startY(setStartY)
	{
		angle = random(360);
		
		vector.x = random(1000)/1000.0;
		vector.y = random(1000)/1000.0;
		vector.z = random(1000)/1000.0;
	};
	Sakura();
	Sakura(const Sakura&);
	Sakura(const Sakura&&);
	void setX(double& sX)
	{
		x = sX;
	};
	void setX(double&& sX)
	{
		x = sX;
	};
	void setY(double& sY)
	{
		y = sY;
	};
	void setY(double&& sY)
	{
		y = sY;
	};
	void setDrawFunction(void (*f)(double,double,double,double,double,double,double))
	{
		sakura = f;
	}
	void draw() const
	{
		sakura(x,y,size,angle,vector.x,vector.y,vector.z);
	}
	const double getX() const
	{
		return x;
	};
	const double getStartX() const
	{
		return startX;
	};
	const double getY() const
	{
		return y;
	};
	const double getStartY() const
	{
		return startY;
	}
	void randomizeAngle()
	{
		angle += pow(-1.0,1.0+random(2))*random(10);
		if (angle > 360)
			angle -= 360;
		else if (angle < 0)
			angle += 360;
	}
	void randomizeVector()
	{
		/********x********/
		vector.x += pow(-1.0,1.0+random(2))*random(100)/100.0;
		if (vector.x > 1.0)
			vector.x -= 1.0;
		else if (vector.x < 0.0)
			vector.x += 1.0;
		/*****************/

		/********y********/
		vector.y += pow(-1.0,1.0+random(2))*random(100)/100.0;
		if (vector.y > 1.0)
			vector.y -= 1.0;
		else if (vector.y < 0.0)
		vector.y += 1.0;
		/*****************/

		/********y********/
		vector.z += pow(-1.0,1.0+random(2))*random(100)/100.0;
		if (vector.z > 1.0)
			vector.z -= 1.0;
		else if (vector.z < 0.0)
			vector.z += 1.0;
		/*****************/
	}
};

class SakuraFlow
{
private:
	std :: vector <Sakura*> masSakura;
	unsigned int count;
	const double step;
	double time;
	bool* isDropped;
	double (*flightFunc)(double,char&&,const int&);
	double (*flyDrop)(double,double,double);
	const int direction;
public:
	SakuraFlow(unsigned int amount,double x,double y,double s,int&& dir):
		count(amount),
		step(s),
		time(0.0),
		direction(dir)
		//masSakura(count,nullptr)
	{
		isDropped = new bool[count];

		for(unsigned int i = 0; i < count; i++)
		{
			isDropped[i] = false;
			masSakura.push_back(new Sakura(0.03 + random(10)/1000.0,x,y));
			//masSakura[i] = new Sakura(0.03 + random(10)/1000.0,x,y);
			masSakura[i]->setX(x);
			masSakura[i]->setY(y);
		}
	};
	SakuraFlow();
	SakuraFlow(const SakuraFlow&);
	SakuraFlow(const SakuraFlow&&);
	void load()
	{
		flyDrop = fall;
		flightFunc = flight;
		for(unsigned int i = 0; i < count; i++)
			masSakura[i]->setDrawFunction(sakura);
	};
	void drop(unsigned int i)
	{
		isDropped[i] = true;
	};
	void draw() const
	{
		double startX,startY;
		for(unsigned int i = 0; i < count; i++)
		{
			if (random(100) > 90)
			{
				masSakura[i]->randomizeVector();
				masSakura[i]->randomizeAngle();
			}
			startY = masSakura[i]->getStartY();
			startX = masSakura[i]->getStartX();
			if(isDropped[i])
			{
				masSakura[i]->setY(flyDrop(startX,time,startY));
				masSakura[i]->draw();
			} else
			{
				masSakura[i]->setX(flightFunc(time - 1*i,'x',direction));
				masSakura[i]->setY(flightFunc(time - 1*i,'y',direction));
				masSakura[i]->draw();
			};
		}
	};
	void stepForward()
	{
		time += step;
	}
};

class BackgroundSakura : public Background
{
private:
	SakuraFlow* leftF;
	SakuraFlow* rightF;
public:
	BackgroundSakura();
	~BackgroundSakura();
	void load();
	void draw() const;
	void correcting();
};

