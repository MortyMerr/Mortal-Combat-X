#pragma once
#include "Alert.h"
#include <GL\GLAux.h>
#include <fstream>
#include "lodepng.h"
#include "defs.h"
#include "BackgroundChild.h"
#include "BackgroundSakura.h"
#include "CircleList.cpp"
#include "Curly.h"
#include "Bagel.h"

class Menu
{
private:
	unsigned slider;
	AUX_RGBImageRec* image[8];
	LPWSTR files[8];
	char* sliderFile;
	int position;
	unsigned int page;
	std :: shared_ptr<Background> backMas[backCount];
	std :: shared_ptr<MainHero> heroMas[heroCount];
	std :: shared_ptr<Text> backText[5];
	std :: shared_ptr<Text> choosing[2];
	int scriptAmount;
	int curScript;
	Alert alert;
	mutable double countNeedBuy;
	CircleList<char>* script;
	bool actionFlag;
	void drawSlider() const;
	void drawBackChoosing() const;
	void drawHeroChosing() const;
public:
	Menu();
	~Menu();
	void load();
	void moveUp();
	void moveDown();
	void draw() const;
	const int getPosition() const;
	const bool&& isChangePageBackground() const;
	const bool&& isChangePageHero() const;
	const bool&& isPageDown() const;
	void toNextPage();
	void toPrevPage();
	void startChangeSettings();
	void action();
	void disAction();
	const bool isAction() const;
	const int startChoosingFirstHero();
	const int startChoosingSecondHero();
	const int endChoosing();
	void correcting() const;
	void startNeedBuy() const;
	void endNeedBuy() const;
};