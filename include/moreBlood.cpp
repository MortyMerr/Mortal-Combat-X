#include "moreBlood.h"

double blood::random(int a)
{
	return rand()%a;
}

void blood::rerandom(int x,int y)
{
	xHit=x;
	yHit=y;
	angle=random(157000)/1000;
	speed= 4+random(10);
	time=0;
}

float blood::straight(float x) 
{
	return 0;
}

float blood::circleFunc(float x,float x0,float r)
{
	return sqrt(r*r - (x-x0)*(x-x0));
}

void blood::part(double tAngle) 
		{
			tAngle *= 180/PI;
			glTranslated(xHit,yHit,0);
			glRotated(tAngle,0,0,1);
			glScalef(size,size,1);

			glBegin(GL_LINES);
			for(double i = 0; i < 0.8*length; i += 0.1)
			{
				double u = sqrt(i);
				double d = straight(i);
				glVertex2d(i,d);
				glVertex2d(i,u);
			}
			
			for(double i = 0.8*length; i < length; i += 0.1)
			{
				double u = radius + circleFunc(i,0.8*length,radius);
				double d = radius - circleFunc(i,0.8*length,radius);
				glVertex2d(i,d);
				glVertex2d(i,u);
			};
			glEnd();

			glScalef(1/size,1/size,1);
			glRotated(-tAngle,0,0,1);
			glTranslated(-xHit,-yHit,0);
		}

void blood::pole()
{
	time += step;
	vX = speed*cos(angle);
	v0Y = speed*sin(angle),vY;
	vY = speed*sin(angle) - 9.81*time;
	curAngle = atan(vY/vX);
	xHit += vX*time;
	yHit += v0Y*time - 9.81*time*time/2.0;
	part(curAngle);
}

void moreBlood::pole()
{
	int yHitMin=0;
	glColor3f(1.0,0.0,0.0);
	for(int k=0;k<howMuchBlood;k++)
	{
		washOfBlood[k]->pole();
		if( washOfBlood[k]->yHit<washOfBlood[yHitMin]->yHit)
			yHitMin=k;
	}
	minY = washOfBlood[yHitMin]->yHit;
}

void moreBlood::fatality()
{
	howMuchBlood=500;
	for(int k=0;k<howMuchBlood;k++)
		washOfBlood[k]->step=0.01;
	fatal=true;
}

void moreBlood::rerandom(int x,int y)
{
	minY = 100;
	for(int i=0;i < howMuchBlood;i++)
		washOfBlood[i]->rerandom(x,y);
}
