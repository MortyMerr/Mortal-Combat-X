#pragma once
#include<iostream>
#include <math.h>
#include "defs.h"
#include "Background.h"
#include "myColor.h"

double sunFunc(double);
double circleFunc(double,double,double);
double greenFunc(double);

class Sun
{
private:
	double x;  //начальное положение
	double y;  
	double speed;
	double radius;
public:
	void draw() const
	{
		glBegin(GL_LINES);
			for (double t = 0.0; t < 2*PI; t += 0.1)
			{
				glColor3d(1.0,1.0,0.0);
				glVertex2d(x,y);
				if (int(ceil(t*10.0)) % 5 == 0)
				{
					glColor3d(1.0,0.843,0.0);
					glVertex2d(x+2.0*radius*cos(t),y+2.0*radius*sin(t));
				} else
				{
					glColor3d(1.0,0.843,0.0);
					glVertex2d(x+radius*cos(t),y+radius*sin(t));
				}
			}
		glEnd();
	};
	void setX(double&& setX)
	{
		x = setX;
	};
	void setY(double&& setY)
	{
		y = setY;
	};
	void setR(double&& r)
	{
		radius = r;
	};
	void setS(double&& s)
	{
		speed = s;
	}
	void stepForward()
	{
		x += 0.01*speed;
		y = sunFunc(x);
	};
	const float getX() const
	{
		return x;
	}
};

class Moon
{
private:
	double x;  //начальное положение
	double y;  
	double speed;
	double radius;
public:
	void draw() const
	{
		//glBegin(GL_LINES);
		glBegin(GL_LINES);
			for (double t = PI/2.0; t < 1.5*PI; t += 0.01)
			{
				glColor3d(1.0,1.0,1.0);
				glVertex2d(x+0.5*radius*cos(t),y+radius*sin(t));
				glColor3d(1.0,0.843,0.0);				
				glVertex2d(x+radius*cos(t),y+radius*sin(t));
			}
		glEnd();
	};
	void setX(double&& setX)
	{
		x = setX;
	};
	void setY(double&& setY)
	{
		y = setY;
	};
	void setR(double&& r)
	{
		radius = r;
	};
	void setS(double&& s)
	{
		speed = s;
	}
	void stepForward()
	{
		x += 0.01*speed;
		y = sunFunc(x);
	};
	const float getX() const
	{
		return x;
	}
};

class BackColor
{
private:
	myColor startDown;
	myColor startUp;
	myColor endDown;
	myColor endUp;
	int num;
	int count;
public:
	BackColor(myColor sD,myColor sU, myColor eD,myColor eU,int c) :
		startDown(sD),
		startUp(sU),
		endDown(eD),
		endUp(eU),
		num(0),
		count(c)
	{};
	myColor getCurrentDownColor()
	{
		double lambda = (double)num++/count;
		return startDown.getPart(lambda,endDown);
	};
	myColor getCurrentUpColor()
	{

		double lambda = (double)num++/count;
		return startUp.getPart(lambda,endUp);
	};

	void recolorForNight()
	{
		startDown = myColor(139.0,62.0,47.0);
		startUp = myColor(139.0,136.0,120.0);
		endDown = myColor(139.0,62.0,122.0);
		endUp = myColor(205.0,205.0,180.0);
		num = 0;
	};
	void recolorForDay()
	{
		startDown = myColor(0.0,154.0,205.0);
		startUp = myColor(164.0, 211.0, 238.0);
		endDown = myColor(1.0,114.0,86.0);
		endUp = myColor(55.0,160.0,122.0);
		num = 0;
	};
};

class Camomile
{
private:
	const double x;
	const double y;
	const double radius;
	const unsigned int petalCounter;
public:
	Camomile();
	Camomile(Camomile&);
	Camomile(double&& setX,double&& setY,double&& r):
		x(setX),
		y(setY),
		radius(r),
		petalCounter(8u + random(15))
	{};
	void draw() const
	{
		double angle;
		glColor3d(1.0,1.0,1.0);
		for (unsigned int i = 0; i < 2*petalCounter+2; i++)
		{
			angle = 360.0/petalCounter*i;
			glTranslated(x,y,0);
			glRotated(angle,0,0,1);

			/**/
			glOval(0,0,radius/3.0,2.0*radius,myColor(1.0,1.0,1.0));
			/**/

			glRotated(-angle,0.0,0.0,1.0);
			glTranslated(-x,-y,0.0);
		}

		glBegin(GL_POLYGON);
			glColor3f(0.9,0.9,0.0);
			for(double t = 0.0; t < 2.0*PI; t += 0.1)
			{
				int p = ceil(t*10.0);

				glVertex2d(x+radius*cos(t),y+radius*sin(t));
			};	
		glEnd();
	}
};

class PoleOfCamomile
{
private:
	Camomile** camomileMas;
	int count;

	PoleOfCamomile();
	PoleOfCamomile(const PoleOfCamomile& X);
public:
	PoleOfCamomile(int amount) :
		count(amount)
	{
		camomileMas = new Camomile*[amount];
		for (int i = 0; i < amount; i++)
			camomileMas[i] = new Camomile(random(gameWidth),random(200),5+random(15));																			
	};
	void draw() const
	{
		for (int i = 0; i < count; i++)
			camomileMas[i]->draw();
	}
};

class BackgroundChild : public Background
{
private:
	enum state {day,night} st;
	std :: shared_ptr<Sun> s;
	std :: shared_ptr<BackColor> bC;
	std :: shared_ptr<Moon> m;
	std :: shared_ptr<PoleOfCamomile> pOC;
	bool isEndOfDay() const;
	bool isEndOfNight() const;
public:
	BackgroundChild();
	~BackgroundChild();
	void correcting();
	void draw() const;
	void load();
};

