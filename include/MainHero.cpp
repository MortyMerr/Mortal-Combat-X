//#include "defs.h"
#include "MainHero.h"

bool MainHero::checkHit( MainHero& pig,coord temp)//��������� ������ �� hitter - pig'�
{
	if ((pow((temp.x-pig.coordOfBody.x),2)/(pig.identity.widthOfBody*pig.identity.widthOfBody) +pow((temp.y-pig.coordOfBody.y),2)/(pig.identity.hightOfBody*pig.identity.hightOfBody)<=1)
		||(pow((temp.x - pig.coordOfBody.x),2)/(pig.identity.HeadRad*pig.identity.widthOfBody)+pow((temp.y-(pig.coordOfBody.y+pig.identity.hightOfBody+pig.identity.HeadRad)),2)/(pig.identity.HeadRad*pig.identity.HeadRad)<=1)
		||
		(((abs(temp.x-pig.coordLeftLeg.x)+abs(pig.coordLeftLeg.x+pig.identity.lengthOfLeg*cos(pig.cornerLeft)-temp.x))    <=(1+pig.identity.lengthOfLeg*cos(pig.cornerLeft)))
		&
		((abs (temp.y-pig.coordLeftLeg.y)+abs(pig.coordLeftLeg.y+(pig.identity.lengthOfLeg+3)*sin(pig.cornerLeft)-temp.y)-4)<=pig.identity.lengthOfLeg))
		||
		(((abs(temp.x-pig.coordRightLeg.x)+abs(pig.coordRightLeg.x-pig.identity.lengthOfLeg*cos(PI-pig.cornerRight)-temp.x))    <=(1+pig.identity.lengthOfLeg*cos(PI-pig.cornerRight)))
		&
		((abs (temp.y-pig.coordRightLeg.y)+abs(pig.coordRightLeg.y+(pig.identity.lengthOfLeg+3)*sin(PI-pig.cornerRight)-temp.y)-4)<=pig.identity.lengthOfLeg)))
		return 1; 
	else 
		return 0;
}

void MainHero :: load()
{
	nameDrawer = std :: shared_ptr<Text>(new Text());

	nameDrawer->setColor(0.0,0.0,0.0,1.0);
	nameDrawer->setFont("font/Ubuntu-R.ttf",18);
	nameDrawer->setText(identity.name);
}

void MainHero :: drawName(const double&& x, const double&& y) const
{
	glPushMatrix();
	glTranslated(x,y,0.0);

	nameDrawer->draw();

	glPopMatrix();
}

const int MainHero::checkDistance(MainHero& anotherHero) const
{
	return abs(coordOfBody.x-anotherHero.coordOfBody.x)-identity.widthOfBody-anotherHero.identity.widthOfBody-0.5*identity.lengthOfHand-0.5*anotherHero.identity.lengthOfHand;
}

void MainHero::changeHealth(int a)
{
	nowHealth-=a;
}

void MainHero::stepRight(MainHero& anotherHero)
{
	if (!inHitLeg == 0) return;
	rightFaced=1;
	if(coordOfBody.x>1100)
	{
		reverse(anotherHero);
	}
	if ((left) && (checkDistance(anotherHero)<-5))
		return;
	for (int i=1;i<identity.speedOfWalking;i++)					//���� ������� ����� ���������� �������� ������. ������ ������ - ����� ��������� �������
	{
		if (rightLegStep)
		{
			if (!inJump)
			{cornerRight+=0.01;
			cornerLeft-=0.01;}
			else 
				coordLeftLeg.x++;
			coordRightLeg.x++;	

			if (cornerRight>=(3.14-acos(0.125)))
			{
				rightLegStep=false;
				leftLegStep=true;
			}
		} else
		{
			if (!inJump){
				cornerRight-=0.01;
				cornerLeft+=0.01;
			}
			else
				coordRightLeg.x=coordRightLeg.x+1.0;
			coordLeftLeg.x=coordLeftLeg.x+1.0;
			if (cornerLeft>=1.57)
			{
				leftLegStep=false;
				rightLegStep=true;
			}
		}
	}

}

void MainHero::stepRight()
{
	if (!inHitLeg == 0) 
		return;
	rightFaced=1;
	for (int i=1;i<identity.speedOfWalking;i++)	
	{
		if (rightLegStep)
		{
			if (!inJump)
			{
				cornerRight+=0.01;
				cornerLeft-=0.01;
			} else 
				coordLeftLeg.x++;
			coordRightLeg.x++;	

			if (cornerRight >= (3.14-acos(0.125)))
			{
				rightLegStep=false;
				leftLegStep=true;
			}
		} else
		{
			if (!inJump){
				cornerRight-=0.01;
				cornerLeft+=0.01;
			}
			else
				coordRightLeg.x=coordRightLeg.x+1.0;
			coordLeftLeg.x=coordLeftLeg.x+1.0;
			if (cornerLeft>=1.57)
			{
				leftLegStep=false;
				rightLegStep=true;
			}
		}
	}
}

void MainHero::reverse(MainHero& anotherHero)
{
	cornerLeft=1.57;
	cornerRight=1.57;
	if(left)
	{

		coordRightLeg.x=gameWidth+200;
		coordLeftLeg.x=coordRightLeg.x-identity.widthOfBody;
	}else
	{
		coordRightLeg.x=-200;
		coordLeftLeg.x=coordRightLeg.x-identity.widthOfBody;
	}
	left=!left;
	anotherHero.left=!anotherHero.left;
}

void MainHero::stepLeft(MainHero& anotherHero)
{
	if (!inHitLeg == 0) return;
	rightFaced=-1;
	if(coordOfBody.x<-100)
	{
		reverse(anotherHero);
	}
	if(!(left) && (checkDistance(anotherHero)<-5))
		return;
	for (int i=1;i<identity.speedOfWalking;i++)
	{
		if (rightLegStep)
		{
			if (!inJump)
			{
				cornerRight-=0.01;
				cornerLeft+=0.01;
			}
			else 
				coordLeftLeg.x=coordLeftLeg.x-1;
			coordRightLeg.x=coordRightLeg.x-1;
			if (cornerRight<=1.57)
			{
				rightLegStep=false;
				leftLegStep=true;
			}
		} else
		{
			if (!inJump)
			{
				cornerRight += 0.01;
				cornerLeft -= 0.01;
			} else coordRightLeg.x=coordRightLeg.x - 1.0;
			coordLeftLeg.x=coordLeftLeg.x - 1.0;
			if (cornerLeft <= acos(0.125))
			{
				leftLegStep = false;
				rightLegStep = true;
			}
		}
	}
}

void MainHero::stepLeft()
{
	if (!inHitLeg == 0)
		return;
	rightFaced=-1;
	for (int i=1;i<identity.speedOfWalking;i++)
	{
		if (rightLegStep)
		{
			if (!inJump)
			{
				cornerRight-=0.01;
				cornerLeft+=0.01;
			}
			else 
				coordLeftLeg.x = coordLeftLeg.x-1;
			coordRightLeg.x = coordRightLeg.x-1;
			if (cornerRight <= 1.57)
			{
				rightLegStep = false;
				leftLegStep = true;
			}
		} else
		{
			if (!inJump)
			{
				cornerRight += 0.01;
				cornerLeft -= 0.01;
			} else coordRightLeg.x = coordRightLeg.x-1.0;
			coordLeftLeg.x = coordLeftLeg.x-1.0;
			if (cornerLeft <= acos(0.125))
			{
				leftLegStep = false;
				rightLegStep = true;
			}
		}
	}
}

void MainHero::jump()
{
	inJump = true;
}

void MainHero :: draw() const
{
	glLineWidth(2.0);

	glBegin(GL_LINES);
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glVertex2d(coordLeftLeg.x,coordLeftLeg.y);																				//����� ����
	glColor3f(identity.color.red-0.5,identity.color.green-0.5,identity.color.bidlo-0.5);
	glVertex2d(coordLeftLeg.x+identity.lengthOfLeg*cos(cornerLeft),coordLeftLeg.y+(identity.lengthOfLeg+3)*sin(cornerLeft));


	/**********rightleg***********/
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glVertex2d(coordRightLeg.x,coordRightLeg.y);
	glColor3f(identity.color.red-0.5,identity.color.green-0.5,identity.color.bidlo-0.5);
	glVertex2d(coordRightLeg.x+identity.lengthOfLeg*cos(cornerRight),coordRightLeg.y+(identity.lengthOfLeg+3)*sin(cornerRight));
	/*****************************/

	glColor3f(identity.color.red-0.5,identity.color.green-0.5,identity.color.bidlo-0.5);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)),coordOfBody.y+identity.hightOfBody*sin(0.5));//������ ����
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)+identity.lengthOfHand/1.414213562375*cos(cornerHittingHand)),coordOfBody.y+identity.hightOfBody*sin(0.5)+identity.lengthOfHand/1.414213562375*sin(cornerHittingHand));
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)+identity.lengthOfHand/1.414213562375*cos(cornerHittingHand)),coordOfBody.y+identity.hightOfBody*sin(0.5)+identity.lengthOfHand/1.414213562375*sin(cornerHittingHand));		
	glColor3f(1.0,0.0,0.0);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)+2*identity.lengthOfHand/1.414213562375*cos(cornerHittingHand)),coordOfBody.y+identity.hightOfBody*sin(0.5));

	glColor3f(identity.color.red-0.5,identity.color.green-0.5,identity.color.bidlo-0.5);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(2.6)),coordOfBody.y+identity.hightOfBody*sin(2.6));//����� ����
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(2.6)+identity.lengthOfHand/3),coordOfBody.y+identity.hightOfBody*sin(2.6)-identity.lengthOfHand/2);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(2.6)+identity.lengthOfHand/3),coordOfBody.y+identity.hightOfBody*sin(2.6)-identity.lengthOfHand/2);
	glColor3f(1.0,0.0,0.0);
	glVertex2d(coordOfBody.x+rightFaced*(identity.widthOfBody*cos(2.6)+identity.lengthOfHand*3/4),coordOfBody.y+identity.hightOfBody*sin(2.6));
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glEnd();

	drawBlood();
}

void MainHero :: correcting()
{
	if (inJump)
	{
		if(ourTime < (speed0/5.1))
		{
			coordLeftLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime);
			coordRightLeg.y=coordLeftLeg.y;
			ourTime+=0.2;
		} else
		{
			ourTime=0;
			inJump=false;
			coordLeftLeg.y=200;
			coordRightLeg.y=coordLeftLeg.y;	
		}
	}


	if (countSound > 0)
		countSound++;
	if (countSound > 15)
	{
		countSound = -1;
		PlaySound(NULL, NULL, SND_ASYNC);
	}
	if (inHit==1)
		if(cornerHittingHand<0)
		{
			cornerHittingHand+=identity.speedOfHit;
			coordRightHand.x=coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)+2*identity.lengthOfHand/1.414213562375*cos(cornerHittingHand));
			coordRightHand.y=coordOfBody.y+identity.hightOfBody*sin(0.5);
		}
		else 
		{
			PlaySound(NULL, NULL, SND_ASYNC);
			inHit=-1;
		};
	if(inHit==-1)
		if(cornerHittingHand>-0.785)
			cornerHittingHand-=identity.speedOfHit;
		else
			inHit=0;
	if (inHitLeg==1)
		if (rightFaced==1)
		{
			if(cornerHittingLeg<2.8)
			{
				cornerHittingLeg+=identity.speedOfHit;
				coordRightLeg.x+=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerRight));
				coordRightLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
				cornerRight=cornerHittingLeg;
			} else 
			{
				inHitLeg=-1;
			}
		} else 
		{
			if (inHitLeg==1)
			{
				if (rightFaced==-1)
				{
					if(cornerHittingLeg>0.34)
					{
						cornerHittingLeg -= identity.speedOfHit;
						coordLeftLeg.x+=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerLeft));
						coordLeftLeg.y= startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
						cornerLeft=cornerHittingLeg;
					} else 
					{
						inHitLeg=-1;
					}
				}
			}
		};
		if (rightFaced==-1)
			if(inHitLeg==-1)
				if(cornerHittingLeg<startLegCorner)
				{
					coordLeftLeg.x-=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerLeft));
					coordLeftLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
					cornerLeft=cornerHittingLeg;
					cornerHittingLeg+=identity.speedOfHit;
				}
				else 
				{
					cornerLeft=startLegCorner; 
					coordLeftLeg.y=coordRightLeg.y; inHitLeg=0;  
					coordLeftLeg.x=startLegX;
				};
		if (rightFaced==1)
			if(inHitLeg==-1)
				if(cornerHittingLeg>startLegCorner)
				{
					coordRightLeg.x-=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerRight));
					coordRightLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
					cornerRight=cornerHittingLeg;
					cornerHittingLeg-=identity.speedOfHit;
				}
				else
				{ 
					cornerRight=startLegCorner;
					coordRightLeg.y=coordLeftLeg.y; inHitLeg=0;  
					coordRightLeg.x=startLegX;
				};
		coordOfBody.x=(coordLeftLeg.x+identity.lengthOfLeg*cos(cornerLeft)+(coordRightLeg.x+identity.lengthOfLeg*cos(cornerRight)-coordLeftLeg.x-identity.lengthOfLeg*cos(cornerLeft))/2);
		coordOfBody.y=identity.lengthOfLeg*sin(cornerLeft)+coordLeftLeg.y-sin(4.365)*identity.hightOfBody;
}

void MainHero :: correcting(MainHero& anotherHero)
{
		if (inJump){
		if(ourTime<(speed0/5.1))
		{
			if(bloody->fatal)
				ourTime+=0.007;
			else			
				ourTime+=0.2;
			coordLeftLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime);
			coordRightLeg.y=coordLeftLeg.y;
		} else
		{
			ourTime=0;
			inJump=false;
			coordLeftLeg.y=200;
			coordRightLeg.y=coordLeftLeg.y;	
		}
	}


	if (countSound > 0)
		countSound++;
	if (countSound > 15)
	{
		countSound = -1;
		PlaySound(NULL, NULL, SND_ASYNC);
	}
	if (inHit==1)
		if(cornerHittingHand<0)
		{
			cornerHittingHand+=identity.speedOfHit;
			coordRightHand.x=coordOfBody.x+rightFaced*(identity.widthOfBody*cos(0.5)+2*identity.lengthOfHand/1.414213562375*cos(cornerHittingHand));
			coordRightHand.y=coordOfBody.y+identity.hightOfBody*sin(0.5);
			if (checkHit(anotherHero, coordRightHand))
			{
				countSound = 1;
				PlaySound(L"sounds/hit.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
				inHit=-1;
				anotherHero.changeHealth(5);
				if(anotherHero.getHealth()==0)
				{
					bloody->fatality();
					bloody->limitOfBlood=-10000;
					identity.speedOfHit=0.005;
				}
				anotherHero.bleeding = true;
				bloody->rerandom(coordRightHand.x,coordRightHand.y);
			}
		}
		else 
		{
			PlaySound(NULL, NULL, SND_ASYNC);
			inHit=-1;
		};
	if(inHit==-1)
		if(cornerHittingHand>-0.785)
			cornerHittingHand-=identity.speedOfHit;
		else
			inHit=0;
	if (inHitLeg==1)
		if (rightFaced==1)
		{
			if(cornerHittingLeg<2.8)
			{
				cornerHittingLeg+=identity.speedOfHit;
				coordRightLeg.x+=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerRight));
				coordRightLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
				cornerRight=cornerHittingLeg;
				if (checkHit(anotherHero, coordRightLeg))
				{
					countSound = 1;
					PlaySound(L"sounds/hit.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
					inHitLeg=-1;
					anotherHero.changeHealth(5);
					if(anotherHero.getHealth()==0)
					{
						bloody->fatality();
						bloody->limitOfBlood=-10000;
						identity.speedOfHit=0.005;
					}
					anotherHero.bleeding = true;
					bloody->rerandom(coordRightLeg.x,coordRightLeg.y);
				}
			} else 
			{
				inHitLeg=-1;
			}
		} else 
		{
			if (inHitLeg==1)
			{
				if (rightFaced==-1)
				{
					if(cornerHittingLeg>0.34)
					{
						cornerHittingLeg -= identity.speedOfHit;
						coordLeftLeg.x+=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerLeft));
						coordLeftLeg.y= startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
						cornerLeft=cornerHittingLeg;
						if (checkHit(anotherHero, coordLeftLeg))
						{
							countSound = 1;
							PlaySound(L"sounds/hit.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
							inHitLeg=-1;
							anotherHero.changeHealth(5);
							if(anotherHero.getHealth()==0)
							{
								bloody->fatality();
								bloody->limitOfBlood=-10000;
								identity.speedOfHit=0.005;
							}
							anotherHero.bleeding=true;
							bloody->rerandom(coordLeftLeg.x,coordLeftLeg.y);
						}
					} else 
					{
						inHitLeg=-1;
					}
				}
			}
		};
		if (rightFaced==-1)
			if(inHitLeg==-1)
				if(cornerHittingLeg<startLegCorner)
				{
					coordLeftLeg.x-=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerLeft));
					coordLeftLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
					cornerLeft=cornerHittingLeg;
					cornerHittingLeg+=identity.speedOfHit;
				}
				else 
				{
					cornerLeft=startLegCorner; 
					coordLeftLeg.y=coordRightLeg.y; inHitLeg=0;  
					coordLeftLeg.x=startLegX;
				};
		if (rightFaced==1)
			if(inHitLeg==-1)
				if(cornerHittingLeg>startLegCorner)
				{
					coordRightLeg.x-=rightFaced*abs(identity.lengthOfLeg*cos(cornerHittingLeg)-identity.lengthOfLeg*cos(cornerRight));
					coordRightLeg.y=startLeftY+speed0*(ourTime)-5*(ourTime)*(ourTime)+identity.lengthOfLeg*sin(startLegCorner)-identity.lengthOfLeg*sin(cornerHittingLeg);
					cornerRight=cornerHittingLeg;
					cornerHittingLeg-=identity.speedOfHit;
				}
				else
				{ 
					cornerRight=startLegCorner;
					coordRightLeg.y=coordLeftLeg.y; inHitLeg=0;  
					coordRightLeg.x=startLegX;
				};
		coordOfBody.x=(coordLeftLeg.x+identity.lengthOfLeg*cos(cornerLeft)+(coordRightLeg.x+identity.lengthOfLeg*cos(cornerRight)-coordLeftLeg.x-identity.lengthOfLeg*cos(cornerLeft))/2);
		coordOfBody.y=identity.lengthOfLeg*sin(cornerLeft)+coordLeftLeg.y-sin(4.365)*identity.hightOfBody;

		if((bleeding) &&  (bloody->minY < bloody->limitOfBlood))
				bleeding=false;
}

void MainHero :: setBlood(std :: shared_ptr<moreBlood> b)
{
	bloody = b;
}

void MainHero :: drawBlood() const
{
	if(bleeding)
	{
		bloody->pole();
	}
}

bool MainHero::getInJump()
{
	return inJump;
}

int MainHero::getHealth()
{
	return nowHealth;
}

int MainHero::getStartHealth()
{
	return identity.startHealth;
}

bool MainHero::getInHit()
{
	if(inHit==0) return 0;
	else return 1;
}

bool MainHero::getInHitLeg()
{
	if(inHitLeg==0) return 0;
	else return 1;
}

MainHero::MainHero(const heroType& a, int t, bool l):
identity(a), 
	inHitLeg(0),
	rightFaced(t),
	left(l),
	nowHealth(a.startHealth),
	rightLegStep(true),
	cornerHittingHand(-0.785),
	inHit(0),
	ourTime(0.0),
	inJump(false), 
	bleeding(false),
	cornerLeft(1.57),
	cornerRight(1.57),
	startLeftY(200.0),
	countSound(0)
{
	speed0=10000/(identity.HeadRad*2+identity.hightOfBody*2+identity.lengthOfLeg);
	if(rightFaced==1)
	{
		coordLeftLeg.x=identity.lengthOfHand;
		coordLeftLeg.y=coordRightLeg.y=200;
		coordRightLeg.x=coordLeftLeg.x+a.widthOfBody;
	}
	if(rightFaced==-1)
	{
		coordRightLeg.x=gameWidth-identity.lengthOfHand;
		coordLeftLeg.y=coordRightLeg.y=200;
		coordLeftLeg.x=coordRightLeg.x-a.widthOfBody;
	}
	coordOfBody.x=(coordLeftLeg.x+identity.lengthOfLeg*cos(cornerLeft)+(coordRightLeg.x+identity.lengthOfLeg*cos(cornerRight)-coordLeftLeg.x-identity.lengthOfLeg*cos(cornerLeft))/2);
	coordOfBody.y=identity.lengthOfLeg*sin(cornerLeft)+coordLeftLeg.y-sin(4.365)*identity.hightOfBody;
}

void MainHero::hitHand()
{	
	inHit=1;
}

void MainHero::hitLeg()
{
	inHitLeg=1;
	if (rightFaced==1) 
	{
		cornerHittingLeg=cornerRight; 
		startLegCorner=cornerRight;
		startLegX=coordRightLeg.x;
	}
	if (rightFaced==-1) 
	{ 
		cornerHittingLeg=cornerLeft; 
		startLegCorner=cornerLeft;
		startLegX=coordLeftLeg.x;
	}
}

const bool MainHero :: isBleeding() const
{
	return bleeding;
}

const std :: string MainHero :: getName() const
{
	return identity.name;
}

MainHero::~MainHero(){};

void MainHero :: drawFeatures() const
{}


