#include "Curly.h"

Curly :: Curly(const heroType& a, int t, bool l):
	MainHero(a,t,l)
{}

void Curly :: draw() const 
{
	printHair();

	MainHero :: draw();
}

void Curly :: drawFeatures() const 
{
	printHair();
}

void Curly :: printHair() const
{
	int  r=4;
	double i;
	heroColor t=identity.color;
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glBegin(GL_POLYGON);
	double step=0.001;
	for(i=0.9; i<3.43;i+=0.01) 
	{//�� ����� �� ����
		glVertex2d(coordOfBody.x+rightFaced*identity.widthOfBody*cos(i),coordOfBody.y+identity.hightOfBody*sin(i));
		t.bidlo-=step;
		t.green-=step;
		t.red-=step;
		glColor3f(t.red,t.green,t.bidlo);
	}
	for(i=2.1;i<5.0;i+=0.01) //����
	{
		glVertex2d(coordOfBody.x+rightFaced*((identity.widthOfBody-5)*cos(3.9)+18*cos(i)),coordOfBody.y+(identity.hightOfBody-5)*sin(3.9)+18*sin(i));
		t.bidlo-=step;
		t.green-=step;
		t.red-=step;
		glColor3f(t.red,t.green,t.bidlo);
	}
	for(i=-1.8;i<0.3;i+=0.01) 
	{//�� ���� �� �����
		glVertex2d(coordOfBody.x+rightFaced*identity.widthOfBody*cos(i),coordOfBody.y+identity.hightOfBody*sin(i));
		t.bidlo-=step;
		t.green-=step;
		t.red-=step;
		glColor3f(t.red,t.green,t.bidlo);
	}
	for(i=-1.1;i<1.6;i+=0.01) 
	{
		glVertex2d(coordOfBody.x+rightFaced*((identity.widthOfBody-5)*cos(0.6)+14*cos(i)),coordOfBody.y+(identity.hightOfBody-5)*sin(0.6)+14*sin(i));
		t.bidlo -= step;
		t.green -= step;
		t.red -= step;
		glColor3f(t.red,t.green,t.bidlo);
	}
	glEnd();
	glColor3f(identity.color.red,identity.color.green,identity.color.bidlo);
	glBegin(GL_POLYGON);
	for(i=0;i<6.28;i+=0.01) 
	{
		glVertex2d(coordOfBody.x+identity.HeadRad*cos(i),coordOfBody.y+identity.hightOfBody+identity.HeadRad-5+identity.HeadRad*sin(i));
		glColor3f(identity.color.red-i*0.1,identity.color.green-i*0.1,identity.color.bidlo-i*0.1);
	}
	glEnd();
	step=0.00007; heroColor temp(139.0,71.0,38.0);
	glColor3f(139.0/255, 71.0/255, 38.0/255);
	glBegin(GL_POINTS);
	for(int i=2;i<6;i++)
	{
		temp.red=139.0/255;
		temp.green=71.0/255;
		temp.bidlo=38.0/255;
		glColor3f(temp.red,temp.green,temp.bidlo);
		for(double t=-49;t<100; t+=0.07){
			glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)   + (i*2.1)*r*cos(t/110.0) ),coordOfBody.y+r*sin(t)+70 -i*5+50*sin(t/100.0) );
			temp.bidlo-=step;
			temp.green-=step;
			temp.red-=step;
			glColor3f(temp.red,temp.green,temp.bidlo);
		}
	}

	for(int i=2;i<4;i++){
		temp.red=139.0/255;
		temp.green=71.0/255;
		temp.bidlo=38.0/255;
		glColor3f(temp.red,temp.green,temp.bidlo);
		for(double t=-10;t<100; t+=0.07){
			glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)  + (i*2.1)*r*cos(t/130.0) ),coordOfBody.y+r*sin(t) +i*5+50*sin(t/90.0) );
			glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)  +20 + (i*2.1)*r*cos(t/80.0) ),coordOfBody.y+r*sin(t)+30 -i*5+50*sin(t/70.0) );
			glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)   + (i*2.1)*r*cos(t/110.0) ),coordOfBody.y+r*sin(t) +i*5+50*sin(t/100.0));
			temp.red=139.0/255;
			temp.bidlo-=step;
			temp.green-=step;
			temp.red-=step;
			glColor3f(temp.red,temp.green,temp.bidlo);
		}
	}
	i=2;
	for(double t=0;t<100; t+=0.07)
	{
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)-40.0  + (i*2.1+10)*r*cos(t/130.0) ),coordOfBody.y+r*sin(t)+40.0 +i*5+7*r*sin(t/60.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t)-43.0  + (i*2.1+10)*r*cos(t/130.0) ),coordOfBody.y+r*sin(t)+45 +i*5+8*r*sin(t/60.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -122+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +65+10*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -124+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +70+10*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -120+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +60+10*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -118+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +55+11*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -118+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +58+12*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -116+(40)*r*cos(t/120.0)),coordOfBody.y+r*sin(t) +53+8*r*sin(t/45.0) );
		glVertex2d(coordOfBody.x-rightFaced*(r*cos(t) -10 + (i*2.1)*r*cos(t/110.0) ),coordOfBody.y+r*sin(t)+20 +i*5+50*sin(t/100.0));
		temp.bidlo-=step;
		temp.green-=step;
		temp.red-=step;
		glColor3f(temp.red,temp.green,temp.bidlo);
	}

	glEnd();
}
