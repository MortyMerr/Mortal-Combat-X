#include "Background.h"


Background::Background(void)
{
}

const int&& Background::printHealthAndName(MainHero& first, MainHero& second) const
{
	
	glColor3f(0.0, 1.0, 0.0);
	glColor3f(0.0, 1.0, 0.0);
	glRectd(10.0,gameHeight*0.9,((gameWidth-40.0)/first.getStartHealth())*first.getHealth()/2.0+10.0, gameHeight*0.93);
	glColor3f(1.0, 0.0, 0.0);
	glRectd(((gameWidth-40.0)/first.getStartHealth())*first.getHealth()/2.0+10.0,gameHeight*0.9,gameWidth/2-10.0,gameHeight*0.93);
	
	glColor3f(0.0, 1.0, 0.0);
	glRectd((gameWidth-40.0)/second.getStartHealth()/2.0*(second.getStartHealth()-second.getHealth())+gameWidth/2.0+10.0, gameHeight*0.9,gameWidth-10.0,gameHeight*0.93);
	glColor3f(1.0, 0.0, 0.0);
	glRectd(gameWidth/2.0+10.0,gameHeight*0.9,(gameWidth-40.0)/second.getStartHealth()/2.0*(second.getStartHealth()-second.getHealth())+gameWidth/2.0+10.0,gameHeight*0.93);

	first.drawName(10.0,gameHeight*0.86);
	second.drawName(gameWidth*0.83,gameHeight*0.86);

	if (second.getHealth()==0) 
	{
		return 1;
	
	} else if (first.getHealth()==0) 
	{
		return 2;		
	} else
		return -1;
}

void Background::printWin(MainHero& winner) const
{
	glPushMatrix();
	glScaled(4.0,4.0,1.0);

	winner.drawName(50.0,10.0);

	glPopMatrix();
}
Background::~Background(void)
{
}

void Background :: correcting() {};

void Background :: load() {};
	
void Background :: draw() const {};

int random(int&& m)
{
	return rand()%m;
}

int random(int& m)
{
	return rand()%m;
}

void glOval(double x,double y,double r1,double r2,myColor c)
{
	glBegin(GL_POLYGON);
		for (double t = 0.0; t < 2*PI; t += 0.1)
		{
			glColor3f(c.r-0.05*t,c.g-0.05*t,c.b-0.05*t);
			glVertex2d(x+r1*cos(t),y+r2*sin(t));
		}
	glEnd();
}
