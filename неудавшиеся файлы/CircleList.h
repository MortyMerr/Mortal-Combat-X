#pragma once

#include<iostream>

template<class T>
struct cell
{
	T inf;
	std :: shared_ptr<cell> next;
};

template<class T>
class CircleList
{
private:
	std :: shared_ptr<cell<T>> last;
public:
	CircleList();
	~CircleList();
	void pushBack(T value);
	void toNext();
	const T getInf() const; 
};

