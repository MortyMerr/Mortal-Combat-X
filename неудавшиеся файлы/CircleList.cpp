#include "CircleList.h"

template<class T>
CircleList<T>::CircleList()
{
	last = nullptr;
}

template<class T>
CircleList<T>::~CircleList()
{
}

template<class T>
void CircleList<T> :: pushBack(T value)
{
	if (last == nullptr)
	{
		last = std :: shared_ptr<cell<T>>(new cell<T>());
		last->inf = value;
		last->next = last;
	} else 
	{

		std :: shared_ptr<cell<T>> first = last->next;

		std :: shared_ptr<cell<T>> n = std :: shared_ptr<cell<T>>(new cell<T>());
		n->inf = value;
		n->next = first;

		last->next = n;

		last = n;
	}
}

template<class T>
void CircleList<T> :: toNext()
{
	last = last->next;
}

template<class T>
const T CircleList<T> :: getInf() const
{
	return last->inf;
}