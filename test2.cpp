#include "include\MainHero.h"
#include "include\BackgroundChild.h"
#include "include\BackgroundSakura.h"
#include "include\Menu.h"

bool KeyDown[256];

heroColor kudrishColor(124, 252, 0);
heroColor bublickColor(135, 206, 235);

heroType first(4,40,100,60,15,200,0.04, kudrishColor,3);//�������, �������� �����, ������ �����, ����� ����, ����� �������, ��������
heroType second(70,50,20,40,30,100,0.1, bublickColor,5);//�������, �����������, �������� �����, ������ ����, ������ �������, ������

MainHero b(first, 1, true,1);
MainHero a(second, -1, false,2);

Background* back;
moreBlood blood;

int numberBack = 1;
int numberFirstHero = 0;
int numberSecondHero = 0;

int menuState = -1;
Menu* menu = new Menu()/* nullptr*/;

void keyPress() 
{
		if(KeyDown['d']==1) b.stepRight(a);
		if(KeyDown['a']==1) b.stepLeft(a); 
		if(KeyDown['w']==1) if (!b.getInJump()) b.jump(); 
		if(KeyDown['e']==1) if (!b.getInHit()) b.hitHand(); 
		if(KeyDown['f']==1) if (!b.getInHitLeg()) b.hitLeg(); 
		if(KeyDown['l']==1) a.stepRight(b);
		if(KeyDown['j']==1) a.stepLeft(b); 
		if(KeyDown['i']==1) if (!a.getInJump()) a.jump(); 
		if(KeyDown['o']==1) if (!a.getInHit()) a.hitHand();
		if(KeyDown[';']==1) if (!a.getInHitLeg()) a.hitLeg(); 
}

void Initialize()
{
	//PlaySound(L"sounds/Back.wav", NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);

	glClearColor(0.0,0.0,0.0,5.0);		
	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();					
	gluOrtho2D(0.0,gameWidth,0.0,gameHeight);

	menu->load();

	switch(numberBack)
	{
	case 0:
		{
			back = new Background();
			break;
		};
	case 1:
		{
			back = new BackgroundChild();
			break;
		}
	case 2:
		{
			back = new BackgroundSakura();
			break;
		}
	}

	back->load();
}

void draw()
{
	keyPress();

	if(menu == nullptr)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		back->correcting();
		back->draw();
		back->printHealth(a,b);

		b.print(a,blood);
		a.print(b,blood);
	
		glutSwapBuffers();

		//��� ����� �������� ����� ���������� �� �������, ����� �� ���� �������� ��� bleeding
		if (!((a.isBleeding()) || (b.isBleeding())))
			Sleep(20);
		else
			Sleep(5);
	} else
	{
		glClear(GL_COLOR_BUFFER_BIT);
		
		menu->draw();

		switch(menuState)
		{
		case -1:
			{
				break;
			};
		case 0:
			{
				break;
			};
		case 1:
			{
				break;
			};
		case 2:
			{
				menu->startChangeSettings();
				menuState = 20;
				menu->disAction();
				break;
			};
		case 3:
			{
				break;
			};
		case 4:
			{
				break;
			};
		case 5:
			{
				break;
			};
		case 6:
			{
				break;
			};
		case 7:
			{
				delete menu;
				menu = nullptr;
				break;
			};
		case 20:
			{
				if (menu->isAction())
				{
					numberBack = menu->startChoosingFirstHero();
					menuState = menu->getPosition();
					menu->disAction();
				}
				break;
			};
		case 21:
			{
				if (menu->isAction())
				{
					numberFirstHero = menu->startChoosingSecondHero();
					menuState = menu->getPosition();
					menu->disAction();
				}
				break;
			};
		case 22:
			{
				if (menu->isAction())
				{
					numberSecondHero = menu->endChoosing();
					menuState - menu->getPosition();
					menu->disAction();
				}
				break;
			}
		}

		glutSwapBuffers();
	}
}

void key(unsigned char key, int x, int y){
	KeyDown[key] = 1;
	
	if (menu != nullptr)
	{
		if(KeyDown['w']==1) 
		{
			menu->moveUp();
			KeyDown['w'] = 0;
		}
		if(KeyDown['s']==1) 
		{
			menu->moveDown();
			KeyDown['s'] = 0;
		}
		if(KeyDown[13] == 1)
		{
			menuState = menu->getPosition();
			menu->action();
			KeyDown[13] = 0;
		} else
		{
			menu->disAction();
		}
		if(KeyDown['d']==1)
		{
			menu->toNextPage();
			KeyDown['d'] = 0;
		}
		if(KeyDown['a']==1)
		{
			menu->toPrevPage();
			KeyDown['a'] = 0;
		}
		draw();
	}
}
void keyUp(unsigned char key, int x, int y){
	KeyDown[key] = 0;
}

int main(int argc, char **argv)
{
	glutInit(&argc,argv);						
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);  
	glutInitWindowSize(gameWidth,gameHeight);				
	glutInitWindowPosition(100,0);			
	glutCreateWindow("Mortal Kombat");				
	Initialize();	
	glutDisplayFunc(draw);			
	glutKeyboardFunc(key);
	glutKeyboardUpFunc(keyUp);
	keyPress();
	glutIdleFunc(draw);
	glutMainLoop();								
	return 0;
}

