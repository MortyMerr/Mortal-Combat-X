#include "include\Menu.h"
#include "include\MainHero.h"
#include "include\BackgroundChild.h"
#include "include\BackgroundSakura.h"
#include "include\Curly.h"
#include "include\Bagel.h"

#include <vector>

std :: vector<int> KeyDown(256);

double GRAPHIC_QUALITY = 0.5;

heroColor kudrishColor(124, 252, 0);
heroColor bublickColor(135, 206, 235);

const heroType first(4,40,100,60,15,200,0.04, kudrishColor,3,"������");//�������, �������� �����, ������ �����, ����� ����, ����� �������, ��������
const heroType second(70,50,20,40,30,100,0.1, bublickColor,5,"������");//�������, �����������, �������� �����, ������ ����, ������ �������, ������

MainHero *b;
MainHero *a;

Background* back;
std :: shared_ptr<moreBlood> blood = std :: shared_ptr<moreBlood>(new moreBlood());

int numberBack = 1;
int numberFirstHero = 0;
int numberSecondHero = 1;

int menuState = -1;
Menu* menu = new Menu() /*nullptr*/;

int iWin;

void keyPress() 
{
		if(KeyDown['d']==1) 
			b->stepRight(*a);
		if(KeyDown['a']==1) 
			b->stepLeft(*a); 
		if(KeyDown['w']==1) 
			if (!b->getInJump())
				b->jump(); 
		if(KeyDown['e']==1) 
			if (!b->getInHit())
				b->hitHand(); 
		if(KeyDown['f']==1)
			if (!b->getInHitLeg()) 
				b->hitLeg(); 
		if(KeyDown['l']==1)
			a->stepRight(*b);
		if(KeyDown['j']==1) 
			a->stepLeft(*b); 
		if(KeyDown['i']==1) 
			if (!a->getInJump()) 
				a->jump(); 
		if(KeyDown['o']==1) 
			if (!a->getInHit()) 
				a->hitHand();
		if(KeyDown[';']==1) 
			if (!a->getInHitLeg())
				a->hitLeg(); 
		if(KeyDown['0']==1)
		{
			if (menu == nullptr)
			{
				menu = new Menu();
				menu->load();
				menuState = -1;
			}
		}
}

void initSettings()
{
	switch(numberBack)
	{
	case 0:
		{
			back = new Background();
			break;
		};
	case 1:
		{
			back = new BackgroundChild();
			break;
		}
	case 2:
		{
			back = new BackgroundSakura();
			break;
		}
	};
	back->load();

	switch(numberFirstHero)
	{
	case 0:
		{
			b = new Bagel(first, 1, true);
			b->setBlood(blood);
			break;
		};
	case 1:
		{
			b = new Curly(second, -1, false);
			b->setBlood(blood);
			break;
		};
	};
	b->load();

	switch(numberSecondHero)
	{
	case 0:
		{
			a = new Bagel(first, 1, true);
			a->setBlood(blood);
			break;
		};
	case 1:
		{
			a = new Curly(second, -1, false);
			a->setBlood(blood);
			break;
		};
	};
	a->load();
}

void Initialize()
{
	glClearColor(1.0,1.0,0.5,5.0);		
	glMatrixMode(GL_PROJECTION);	
	glLoadIdentity();
	glPointSize(1.0/GRAPHIC_QUALITY);
	gluOrtho2D(0.0,gameWidth,0.0,gameHeight);
	if (menu != nullptr)
		menu->load();
	else
		initSettings();
}

void draw()
{
	keyPress();

	if(menu == nullptr)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		back->correcting();
		back->draw();
		iWin = back->printHealthAndName(*a,*b);

		b->draw();
		a->draw();

		b->correcting(*a);
		a->correcting(*b);

		if (iWin == 1)
			back->printWin(*a);
		else if (iWin == 2)
			back->printWin(*b);
	
		glutSwapBuffers();

		/*if (!((a->isBleeding()) || (b->isBleeding())))
			Sleep(30);
		else
			Sleep(20);*/
	} else
	{
		glClear(GL_COLOR_BUFFER_BIT);
		
		menu->draw();

		menu->correcting();

		switch(menuState)
		{
		case -1:
			{
				break;
			};
		case 0:
			{
				if (menu->isAction())
					exit(0);
				break;
			};
		case 1:
			{
				if (menu->isAction())
				{
					menu->startNeedBuy();
					//todo
					menu->disAction();
				}
				break;
			};
		case 2:
			{
				menu->startChangeSettings();
				menuState = 20;
				menu->disAction();
				break;
			};
		case 3:
			{
				if (menu->isAction())
				{
					menu->startNeedBuy();
					//todo
					menu->disAction();
				}
				break;
			};
		case 4:
			{
				if (menu->isAction())
				{
					menu->startNeedBuy();
					//todo
					menu->disAction();
				}
				break;
			};
		case 5:
			{
				if (menu->isAction())
				{
					menu->startNeedBuy();
					//todo
					menu->disAction();
				}
				break;
			};
		case 6:
			{
				delete menu;
				menu = nullptr;
				/***/
				initSettings();
				/**/
				break;
			};
		case 7:
			{
				if (menu->isAction())
				{
					menu->startNeedBuy();
					//todo
					menu->disAction();
				}
				break;
			};
		case 20:
			{
				if (menu->isAction())
				{
					numberBack = menu->startChoosingFirstHero();
					menuState = menu->getPosition();
					menu->disAction();
				}
				break;
			};
		case 21:
			{
				if (menu->isAction())
				{
					numberFirstHero = menu->startChoosingSecondHero();
					menuState = menu->getPosition();
					menu->disAction();
				}
				break;
			};
		case 22:
			{
				if (menu->isAction())
				{
					numberSecondHero = menu->endChoosing();
					menuState = menu->getPosition();
					menu->disAction();
				}
				break;
			}
		}
		glutSwapBuffers();
		Sleep(15);
	}
}

void key(unsigned char key, int x, int y){
	KeyDown[key] = 1;
	if (menu != nullptr)
	{
		if(KeyDown['w'] == 1) 
		{
			menu->moveUp();
			KeyDown['w'] = 0;
		}
		if(KeyDown['s'] == 1) 
		{
			menu->moveDown();
			KeyDown['s'] = 0;
		}
		if(KeyDown[13] == 1)
		{
			menuState = menu->getPosition();
			menu->action();
			KeyDown[13] = 0;
		} else
		{
			menu->disAction();
		}
		if(KeyDown['d'] == 1)
		{
			menu->toNextPage();
			KeyDown['d'] = 0;
		}
		if(KeyDown['a'] == 1)
		{
			menu->toPrevPage();
			KeyDown['a'] = 0;
		}
		draw();
	}
}
void keyUp(unsigned char key, int x, int y){
	KeyDown[key] = 0;
}

int main(int argc, char **argv)
{
	glutInit(&argc,argv);						
	glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);  
	glutInitWindowSize(gameWidth,gameHeight);				
	glutInitWindowPosition(100,0);			
	glutCreateWindow("Mortal Kombat");				
	Initialize();	
	glutDisplayFunc(draw);			
	glutKeyboardFunc(key);
	glutKeyboardUpFunc(keyUp);
	keyPress();
	glutIdleFunc(draw);
	glutMainLoop();								
	return 0;
}

